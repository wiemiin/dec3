(function () {
  angular
    .module('paApp')
    .controller('WelcomeCtrl', WelcomeCtrl)

  WelcomeCtrl.$inject = ["paAppAPI", "$state", "$scope", "$filter", "$window", 'user'];

  function WelcomeCtrl(paAppAPI, $state, $scope, $filter, $window, user) {
    var self = this;

    self.user = user;
    self.client = {};
    self.clients = [];
    self.booking = {};
    self.bookings = [];
    self.current = {};
    self.message = "";
    self.radio = "client";
    self.msg = '';

    self.showResult = false;

    self.searchClientName = function () {


      paAppAPI.searchClientName(self.term)
        .then(function (result) {
          console.log(result);
          if(result.length === 0) {
            self.msg = 'CLIENT DOES NOT EXISTS';
          }
          console.log(result);
          self.clients = result;

        }).catch(function (err) {
          console.log(err);
          if (err.status == 404) {
            self.message = "No clients found";

          }
        });

    };
    self.searchAllClients = function () {

      self.showResult = false;
      self.showClient = true;

      paAppAPI.searchAllClients()
        .then(function (result) {
          console.log(result);

          self.clients = result;

        }).catch(function (err) {
          console.log(err);
          if (err.status == 404) {
            self.message = "No clients found";

          }
        });

    }


    $scope.reloadRoute = function () {
      console.log("Reloading...");
      $window.location.reload();
    }

    self.addClient = function () {
      self.showSuccessMessage = false;
      self.showFailureMessage = false;

      console.log("Adding client... ");
      console.log(self.client.firstname);
      console.log(self.client.lastname);
      console.log(self.client.mobile);

      paAppAPI.addClient(self.client)
        .then(function (result) {
          console.log(result);
          self.message = result.data;
          self.showSuccessMessage = true;
          self.clients.unshift({
            'firstname': self.client.firstname,
            'lastname': self.client.lastname,
            'mobile': self.client.mobile
          });

          if (self.addClientForm) {
            self.addClientForm.$setPristine();
            self.addClientForm.$setUntouched();
            self.client = {};
          }

        }).catch(function (err) {
          console.log(err);
          self.message = err.data;
          self.msg = 'CLIENT ALREADY EXISTS';
          self.showFailureMessage = true;
        });
    };

    self.editClient = function (id) {

      console.log("Editing client... ");

      paAppAPI.getClientById(id)
        .then(function (result) {
          console.log(id);
          console.log(result);
          self.message = result;
        }).catch(function (err) {
          console.log(err)
        });

    }

    self.saveClient = function (client) {

      console.log("Saving client... ");

      paAppAPI.updateClient(client)
        .then(function (result) {
          console.log(result);

          self.message = result.data;
          self.showSuccessMessage = true;

          if (self.addClientForm) {
            self.addClientForm.$setPristine();
            self.addClientForm.$setUntouched();
            self.client = {};
          }

        }).catch(function (err) {
          console.log(err);
          self.message = err.data;
          self.showFailureMessage = true;
        });

    }

    self.deleteClient = function (id) {
      console.log("Deleting client... ");

      paAppAPI.deleteClient(id)
        .then(function (result) {
          console.log(id);
          console.log(result);
          self.message = result;

        }).catch(function (err) {
          console.log(err);
        });
    };

    self.removeRow = function (clientIndex) {
      self.clients.splice(clientIndex, 1);
    };

    //// BOOKING

    self.searchAllBookings = function () {

      paAppAPI.searchAllBookings(self.term)
        .then(function (result) {
          console.log(result);
          self.bookings = result;

          for (var i = 0; i < self.bookings.length; i++) {
            self.bookings[i].datetime = (self.bookings[i].datetime) ? new Date(self.bookings[i].datetime) : new Date();

          }

        }).catch(function (err) {
          console.log(err);
          if (err.status == 404) {
            self.message = "No bookings found";
          }
        });
    }

    self.searchBookingByName = function () {

      paAppAPI.searchBookingByName(self.term)
        .then(function (result) {
          if(result.length === 0) {
            self.msg = 'CLIENT DOES NOT EXISTS';
          }
          console.log(result);
          self.bookings = result;
        
        }).catch(function (err) {
          console.log(err);
          if (err.status == 404) {
            self.message = "No bookings found";
          }
        });

    };

    self.addBooking = function () {

      console.log("Adding booking... ");
      console.log(self.booking.training);
      console.log(self.booking.firstname);
      console.log(self.booking.lastname);
      console.log(self.booking.datetime);
      console.log(self.booking.notification);

      self.showResult = true;

      paAppAPI.addBooking(self.booking)
        .then(function (result) {
          console.log(result);
          self.message = result.data;
          self.showSuccessMessage = true;
          self.bookings.unshift({
            'training': self.booking.training,
            'firstname': self.booking.firstname,
            'lastname': self.booking.lastname,
            'datetime': self.booking.datetime,
            'notification': self.booking.notification
          });

          if (self.addBookingForm) {
            self.addBookingForm.$setPristine();
            self.addBookingForm.$setUntouched();
            self.booking = {};
          }

        }).catch(function (err) {
          console.log(err);
          self.message = err.data;
          self.showFailureMessage = true;
        });
    }





  }

})();