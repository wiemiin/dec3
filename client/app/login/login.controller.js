(function() {
    angular
      .module('paApp')
      .controller('LoginCtrl', LoginCtrl);
  
    LoginCtrl.$inject = [ 'PassportSvc', '$state' ];
  
    function LoginCtrl(PassportSvc, $state) {
      var self = this;
  
      self.user = {
        username: '',
        password: '',
      }

      self.msg = '';

      self.login = login; 
      
          function login() { 
            PassportSvc.login(self.user)
              .then(function(result) {
                $state.go('welcome.add');
                return true;
              })
              .catch(function(err) {
                self.msg = 'INVALID EMAIL OR PASSWORD';
                self.user.username = self.user.password = '';
                return false;
              });
          }


    }
  })();