(function () {

    angular
        .module("paApp")
        .config(uirouterAppConfig)
    uirouterAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function uirouterAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state("home", {
                url: "/home",
                templateUrl: "/app/home/home.html",
                controller: 'HomeCtrl',
                controllerAs: 'ctrl'
            })
            .state("signup", {
                url: "/signup",
                templateUrl: "/app/signup/signup.html",
                controller: 'SignupCtrl',
                controllerAs: 'ctrl',
                authenticate: true
                
            })
            // nested list with custom controller
            .state('signup.user', {
                url: '/user',
                templateUrl: '/app/signup/user.html',
                controller: 'SignupCtrl',
                controllerAs: 'ctrl'
            })
            // nested list with just some random string data
            .state('signup.clients', {
                url: '/clients',
                templateUrl: '/app/signup/clients.html',
                controller: 'SignupCtrl',
                controllerAs: 'ctrl'
            })
            // nested list with just some random string data
            .state('signup.schedules', {
                url: '/schedules',
                templateUrl: '/app/signup/schedules.html',
                controller: 'SignupCtrl',
                controllerAs: 'ctrl'
            })
            .state('signup.report', {
                url: '/report',
                templateUrl: '/app/signup/report.html',
                controller: 'SignupCtrl',
                controllerAs: 'ctrl'
            })
            .state("login", {
                url: "/login",
                templateUrl: 'app/login/login.html',
                controller: 'LoginCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                        }
                    },
            })
            .state("welcome", {
                url: "/welcome",
                templateUrl: 'app/welcome/welcome.html',
                controller: 'WelcomeCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                        }
                    },
            })
            .state("welcome.add", {
                url: "/add",
                templateUrl: 'app/welcome/add.html',
                controller: 'WelcomeCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                        }
                    },

            })
            .state("welcome.edit", {
                url: "/edit",
                templateUrl: 'app/welcome/edit.html',
                controller: 'WelcomeCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                        }
                    },

            });

        $urlRouterProvider.otherwise("/home");
    }

})();