(function() {
    angular
      .module('paApp')
      .controller('HomeCtrl', HomeCtrl);
  
      HomeCtrl.$inject = ['$state', '$scope', '$location', '$anchorScroll', '$timeout'];
      
        function HomeCtrl($state, $scope, $location, $anchorScroll, $timeout) {
          var self = this;
      
          self.gotoWhat = function (what) {
            $location.hash('what');
            $anchorScroll();
          };
      
          self.gotoHow = function (how) {
            $location.hash('how');
            $anchorScroll();
          };
      
          self.gotoPlans = function (plans) {
            $location.hash('plans');
            $anchorScroll();
          };
      
        }
    
  })();