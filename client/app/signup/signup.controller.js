(function () {
    "use strict";
    angular.module("paApp").controller("SignupCtrl", SignupCtrl);

    SignupCtrl.$inject = ["paAppAPI", "$state"];

    function SignupCtrl(paAppAPI, $state) {
        var self = this;

        self.getStarted = false;

        self.user = {

        };

        self.showSuccessMessage = false;
        self.showFailureMessage = false;
        self.message = "";

        self.init = function () {
            self.showSuccessMessage = false;
            self.showFailureMessage = false;
        }

        self.init();

        self.addUser = function () {
            self.showSuccessMessage = false;
            self.showFailureMessage = false;

            self.getStarted = true;
            
            console.log("Adding user... ");
            console.log(self.user.email);
            console.log(self.user.name);

            paAppAPI.addUser(self.user)
                .then(function (result) {
                    console.log(result);
                    self.message = result.data;
                    self.showSuccessMessage = true;
                }).catch(function (err) {
                    console.log(err);
                    self.message = err.data;
                    self.showFailureMessage = true;
                });
        }

        self.login = function () {
            $state.go("login");
        }
    }    

})();