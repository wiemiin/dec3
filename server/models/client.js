// var mongoose = require('mongoose');

// module.exports = mongoose.model('Client', {
// 	id: String,
// 	firstname: String,
// 	lastname: String,
// 	mobile: String
// });


var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var clientSchema = new Schema({

	id: String,
	firstname: String,
	lastname: String,
	mobile: String,
	user: {type: Schema.Types.ObjectId, ref: 'User'}
	
});

module.exports = mongoose.model('Client', clientSchema);