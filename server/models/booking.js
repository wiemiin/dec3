var mongoose = require('mongoose');

module.exports = mongoose.model('Booking', {
    id: String,
    training: String,
	firstname: String,
	lastname: String,
    datetime: Date,
    notification: String
});