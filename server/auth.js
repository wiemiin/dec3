//import and inform passport to use local strategy
var LocalStrategy = require('passport-local').Strategy; 
var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
var User = require('./models/user');
var bCrypt = require('bcrypt-nodejs');
var config = require('./config');


//rules of passport, implement functions that it requires 

//authenticateUser is a callback, not mandatory (2)
//this depends on localAuthenticate
/* function authenticateUser(username, password) {
  var userDB = config.USER_DATABASE;

  for (var i=0; i<userDB.length; i++) {
    var currentUser = userDB[i];
    if (username == currentUser.username) {
      return (password == currentUser.password);
    }
  }

  return false;
}
 */
module.exports = function(app, passport) {

  //this is callback  (3) 
  // var localAuthenticate = function(req, username, password, done) {
  //   console.log(username);

  //   username = username.toLowerCase();

//     User.findOne({ 'email' :  username }, 
//         function(err, user) {
//             console.log(user);
//             // In case of any error, return using the done method
//             if (err)
//                 return done(err);
//             // Username does not exist, log the error and redirect back
//             if (!user){
//                 console.log('User Not Found with username ' + username);
//                 return done(null, false);                 
//             }   //return done(null, false, req.flash('message', 'User Not found.'));     
//             // User exists but wrong password, log the error 
//             if (!isValidPassword(user, password)){
//                 console.log('Invalid Password');
//                 return done(null, false); // redirect back to login page
//             }   //return done(null, false, req.flash('message', 'Invalid Password'));
//             // User and password both match, return user from done method
//             // which will be treated like success
//             return done(null, user);
//         }
//     );
//   };
  
  //this is MANDATORY. this will call serialize  (1)
  passport.use(new LocalStrategy(
    { // redefine the field names the stratgey (passport-local) expects
      usernameField: 'username',
      passwordField: 'password',
      passReqToCallback : true
    }, function(req, email, password, done) {

        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function() {

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ 'local.email' :  email }, function(err, user) {
            // if there are any errors, return the error
            console.log("This is from auth.js " + user);
            console.log("This is from auth.js " + email);
            console.log("This is from auth.js " + password);
            // In case of any error, return using the done method
            if (err)
                return done(err);
            // Username does not exist, log the error and redirect back
            if (!email){
                console.log(email + ' does not exists');
                return done(null, false);                 
            }   //return done(null, false, req.flash('message', 'User Not found.'));     
            // User exists but wrong password, log the error 
            if (!validPassword(user, password)){
                console.log('Invalid Password');
                return done(null, false); // redirect back to login page
            }  //return done(null, false, req.flash('message', 'Invalid Password'));
            // User and password both match, return user from done method
            // which will be treated like success
            return done(null, user);

        });    

        });

    }
  
    //localAuthenticate // the strategy's "verify" callback
  ));

//   function verifyCallback(accessToken, refreshToken, profile, done) {
//     console.log("Coming from Google....");
//     if (profile.provider === 'google' || profile.provider === 'facebook') {
//       id = profile.id;
//       email = profile.emails[0].value;
//       displayName = profile.displayName;
//       provider_type = profile.provider;
//       // insert to db using sequelize
//       done(null, email);
//   }else{
//       done(null, false);
//   }
// }

  passport.use(new GoogleStrategy({
    clientID: config.Google_key,
    clientSecret: config.Google_secret,
    callbackURL: config.Google_callback_url
}, function(token, refreshToken, profile, done) {
  
          // make the code asynchronous
          // User.findOne won't fire until we have all our data back from Google
          process.nextTick(function() {

              // try to find the user based on their google id
              User.findOne({ 'google.id' : profile.id }, function(err, user) {
                  if (err)
                      return done(err);
  
                  if (user) {
  
                      // if a user is found, log them in
                      return done(null, user);
                  } else {
                      // if the user isnt in our database, create a new user
                      var newUser          = new User();
  
                      // set all of the relevant information

                      newUser.google.id    = profile.id;
                      newUser.google.token = token;
                      newUser.google.name  = profile.displayName;
                      newUser.google.email = profile.emails[0].value; // pull the first email
  
                      // save the user
                      newUser.save(function(err) {
                          if (err)
                              throw err;
                          return done(null, newUser);
                      });
                  }
              });
          });
  
      }));

  
  passport.serializeUser(function(user, done) {
    //console.log('passport.serializeUser: ' + username);
    done(null, user.id);
  });
  
  passport.deserializeUser(function(id, done) {
    //console.log('passport.deserializeUser: ' + user.id);
    User.findById(id, function(err, user) {
        console.log('deserializing user:', user);
        done(err, user);
    });
  });
  
  var isAuthenticated = function(req, res, next) {
    //console.log("isAuthenticated(): ", req.user);
    if (req.isAuthenticated()) {
      next(); //good moves to the next one 
    }
    else {
      res.sendStatus(401);
    }
  }

  // var isValidPassword = function(user, password){
  //     return bCrypt.compareSync(password, this.local.password);
  // }

  var bcrypt   = require('bcrypt-nodejs');

  var validPassword = function(user, password) {

    if(user === null) {
        
    } else {
        return bcrypt.compareSync(password, user.local.password);
    }

};

  return {
    isAuthenticated: isAuthenticated,
  }
};