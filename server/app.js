"use strict";
console.log("Starting...");

require('dotenv').config();

var express = require("express");
var app = express();

//var morgan       = require('morgan');
var cookieParser = require('cookie-parser');

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.PORT || 3000;

//app.use(express.logger('dev')); // log every request to the console
//app.use(express.cookieParser()); // read cookies (needed for auth)

var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');
var config = require('./config');
var PORT = config.PORT;
var mongoose = require('mongoose');
// Connect to DB
mongoose.connect(config.Mongodb_url, { useMongoClient: true });
mongoose.Promise = global.Promise;

app.use(session({
    secret: config.SECRET,
    resave: true,
    saveUninitialized: true,
}));

app.use(express.static(__dirname + "/../client/"));

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

var auth = require('./auth')(app, passport);
require('./routes')(auth, app, passport);

app.use(function (req, res) {
    res.send("<h1>Page Not Found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

module.exports = app;