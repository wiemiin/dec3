'use strict';

module.exports = {
  PORT: process.env.PORT,

  SECRET: process.env.SESSION_SECRET,
  Google_key: process.env.GOOGLE_KEY,
  Google_secret: process.env.GOOGLE_SECRET,
  Google_callback_url: process.env.GOOGLE_CALLBACK_URL,
  Mongodb_url: process.env.MONGOB_URL,
  Twilio_sid: process.env.TWILIO_SID,
  Twilio_token: process.env.TWILIO_TOKEN

};