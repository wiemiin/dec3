const WELCOME = "/#!/welcome/add";
const LOGIN = "/#!/login";

var User = require('./models/user');
var Client = require('./models/client');
var Booking = require('./models/booking');
var Twilio = require('twilio');

var moment = require('moment');
moment().format();
var bcrypt = require('bcrypt-nodejs');
var config = require('./config');
var frequencyMilliseconds = 60000;

//TEST ACCOUNT 
var accountSid = "AC78fd472f244e86012a2e30743a7bb281";
var authToken = "b1280341c7ec13cf0bd0a75b973e38e5";


module.exports = function (auth, app, passport) {

  var generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
  };

  //LOCAL USER

  app.post('/login', passport.authenticate('local'), function (req, res) {
    console.log("passport user" + req.user);
    res.status(200).send({
      user: req.user
    });

  });

  app.get('/user/auth', auth.isAuthenticated, function (req, res, next) {
    if (req.user) {
      res.status(200).json({
        user: req.user
      });
    } else {
      res.sendStatus(401);
    }
  });

  //GOOGLE USER

  app.get("/oauth/google", passport.authenticate("google", {
    scope: ["email", "profile"]
  }));

  app.get("/oauth/google/callback", passport.authenticate("google", {
    successRedirect: WELCOME,
    failureRedirect: LOGIN
  }));

  app.get('/logout', function (req, res) {
    req.logOut();
    res.redirect('/');
  });

  /**
   * Register USER
   */
  app.post("/api/user", function (req, res) {
    const user = req.body;
    console.log(user);

    User.findOne({
        'local.email': user.email
      },
      function (err, result) {
        if (err) {
          console.log(err);
          handleError(err, res);
          return;
        }
        if (result) {
          res.status(500).send("Email already exists in database");
        } else {
          var newUser = new User();
          newUser.local.password = generateHash(user.password);
          newUser.local.email = user.email;
          newUser.local.name = user.name;
          newUser.local.mobile = user.mobile;
          newUser.save(function (err, result) {
            res.status(201).send("User added to database");
          });
        }

      });
  });


  /**
   * Register CLIENT
   */
  app.post("/api/client", function (req, res) {
    const client = req.body;
    console.log(client);

    Client.findOne({
        $and: [{
            firstname: client.firstname
          },
          {
            lastname: client.lastname
          }
        ]
      },
      function (err, result) {
        if (err) {
          console.log(err);
          handleError(err, res);
          return;
        }
        if (result) {
          res.status(500).send({
            msg: "Client already exists in database"
          });
        } else {
          var newClient = new Client();
          newClient.firstname = client.firstname;
          newClient.lastname = client.lastname;
          newClient.mobile = client.mobile;
          newClient.save(function (err, result) {
            res.status(201).send("Client added to database");
          });
        }

      });

  });

  /**
   * EDIT client
   */

  app.get("/api/client/:id", function (req, res) {
    const id = req.params.id;
    console.log(req.params.id);

    Client.findOne({
      "_id": req.params.id
    }, (err, result) => {
      if (err) {
        console.log(err);
        handleError(err, res);
        return;
      }

      res.status(200).json({
        msg: 'Start editing'
      });
    });
  });

  /**
   * SAVE client
   */

  app.put("/api/client/:id", function (req, res) {

    console.log(req.params.id);
    console.log(req.body.client);

    Client.updateOne({
      "_id": req.params.id
    }, {
      $set: req.body.client
    }, (err, result) => {
      if (err) {
        console.log(err);
        handleError(err, res);
        return;
      }

      res.status(200).json({
        msg: 'Successfully saved'
      });
    });
  });

  /**
   * DELETE client
   */

  //route
  app.delete("/api/client/:id", function (req, res) {
    console.log(req.params.id);
    //const id = req.params.id.toString(); //to convert type 
    //console.log("Deleting " + id);

    //controller
    Client.deleteOne({
      "_id": req.params.id
    }, function (err, result) {
      if (err) {
        console.log(err);
        handleError(err, res);
        return;
      }
      res.status(200).json({
        msg: 'Successfully deleted'
      });
    });

  });

  /**
   * SEARCH client name
   */

  app.get("/api/client?", function (req, res) {

    console.log("Search > " + req.query);
    var keyword = req.query.keyword;

    Client.find({
      "firstname": new RegExp('^' + keyword + '$', "i")
    }, (err, result) => {
      if (err) {
        console.log(err);
      }
      res.status(200).json(result);


    });
  });

  /**
   * SEARCH all clients
   */

  app.get("/client", function (req, res) {

    Client.find({}, (err, result) => {
      if (err) {
        console.log(err);
      }
      res.status(200).json(result);

    });
  });

  /**
   * Register BOOKING
   */
  app.post("/api/booking", function (req, res) {
    const booking = req.body;
    console.log(booking);


    Booking.create(req.body, function (error, result) {
      console.log(req.body);
      if (error) {
        console.log(error);
      }
      res.status(201).send("New booking added to database");


    });
  });


  /**
   * SEARCH bookings by name
   */

  app.get("/api/booking?", function (req, res) {

    console.log("Search booking > " + req.query.keyword);
    var keyword = req.query.keyword;

    Booking.find({
      "firstname": new RegExp('^' + keyword + '$', "i")
    }, (err, result) => {
      if (err) {
        console.log(err);
      }
      res.status(200).json(result);

    });
  });


  /**
   * SEARCH all bookings
   */

  app.get("/booking", function (req, res) {

    Booking.find({}, (err, result) => {
      if (err) {
        console.log(err);
      }
      res.status(200).json(result);

    });
  });

  /**
   * Create REMINDERS
   */

  setInterval(function () {
    var timeNow = new Date();
    //console.log(Math.floor(timeNow.getTime()));
    //{$lt: Math.floor(timeNow.getTime())}

    // Find reminders and check every 1 min to see if any message to be sent out
    Booking.find({
      "datetime": timeNow
    }, function (err, reminders) {
      if (err) {
        console.log(err);
        return;
      }

      if (reminders.length == 0) {
        console.log('No messages to be sent');
        return;
      }

      const client = new Twilio(accountSid, authToken);

      //+15005550006 is for test

      reminders.forEach(function (message) {
        client.messages.create({
          body: "Hi " + message.firstname + ", get ready to work out tomorrow at " + message.datetime,
          to: "+65" + message.mobile,
          from: '+15005550006'
        }, function (err, sms) {
          if (err)
            console.log(err);

          console.log('Sending to user' + message.firstname + " " + message.mobile);
          process.stdout.write(sms.sid);
        });

      });
    });
  }, frequencyMilliseconds);


};